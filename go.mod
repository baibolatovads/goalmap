module gitlab.com/baibolatovads/goalmap

go 1.12

require (
	github.com/go-kit/kit v0.10.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	gopkg.in/olivere/elastic.v5 v5.0.84
)
