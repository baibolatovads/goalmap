package main

import (
	"flag"
	"fmt"
	"gitlab.com/baibolatovads/goalmap/src/config"
	"gitlab.com/baibolatovads/goalmap/src/goalMap"
	"gitlab.com/baibolatovads/goalmap/src/repository/elasticDB"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var (
	goalMapService goalMap.Service
)

func main(){
	// Set http port
	httpAddr := flag.String("http.addr", ":9200", "HTTP listen address only port :9200")
	flag.Parse()

	// Get configs
	err := config.GetConfigs()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}


	// Init connections
	elasticConnection, err := elasticDB.ElasticConnectionStart()
	if err != nil {
		panic(fmt.Errorf("Elastic connection error: %s \n", err))
	}

	goalCommandRepo := elasticDB.NewGoalCommandRepo(elasticConnection)
	goalMapCommandRepo := elasticDB.NewGoalMapCommandRepo(elasticConnection)

	goalMapService = goalMap.NewService(goalCommandRepo, goalMapCommandRepo)

	mux := http.NewServeMux()
	mux.Handle("/goal-map/", goalMap.MakeHandler(goalMapService))

	http.Handle("/goal-map/", accessControl(mux))

	errs := make(chan error, 2)

	// Init http serve
	go func() {
		errs <- http.ListenAndServe(*httpAddr, nil)
	}()

	// Listen errors chan
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	fmt.Println("terminated", <-errs)

}

// Additional structures & functions
func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization,X-Owner,darvis-dialog-id")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
