package elasticDB

import (
	"gitlab.com/baibolatovads/goalmap/src/config"
	"gitlab.com/baibolatovads/goalmap/src/errors"
	"gopkg.in/olivere/elastic.v5"
)

func ElasticConnectionStart() (*elastic.Client, error) {

	client, err := elastic.NewClient(elastic.SetURL(config.AllConfigs.Elastic.ConnectionUrl...), elastic.SetSniff(true))
	if err != nil {
		return nil, errors.ElasticConnectError.DevMessage(err.Error())
	}

	return client, nil
}
