package elasticDB

import (
	"context"
	"encoding/json"
	"gitlab.com/baibolatovads/goalmap/src/domain"
	"gitlab.com/baibolatovads/goalmap/src/errors"
	"gopkg.in/olivere/elastic.v5"
)

type GoalCommandRepo struct{
	client *elastic.Client
}

func NewGoalCommandRepo(client *elastic.Client) domain.GoalCommandRepo{
	return &GoalCommandRepo{client: client}
}

type GoalMapCommandRepo struct{
	client *elastic.Client
}

func NewGoalMapCommandRepo(client *elastic.Client) domain.GoalMapCommandRepo{
	return &GoalMapCommandRepo{client: client}
}


func (r *GoalCommandRepo) GetById(id string) (*domain.Goal, error) {
	get, err := r.client.Get().
		Index("map").
		Id(id).
		Do(context.TODO())

	if err != nil {
		errors.NoContentFound.DeveloperMessage = "No resource"
		return nil, errors.NoContentFound
	}

	var goal domain.Goal

	if get.Found {
		err := json.Unmarshal(*get.Source, &goal)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, err
	}
	return &goal, err
}


func (r *GoalCommandRepo) EditGoal(action string, goal *domain.Goal) error{
	if goal.Id == ""{
		goal.GenerateId()
		_, err := r.client.Index().
			Index("map").
			Type("goal").
			Id(goal.Id).
			BodyJson(goal).
			Refresh("true").
			Do(context.TODO())

		return err
	}else {
		if action == "mark_done" {
			_, err := r.client.Update().
				Index("map").
				Type("goal").
				Id(goal.Id).
				Script(elastic.NewScriptInline("ctx._source.status = params.status").
					Lang("painless").
					Param("status", true)).
				Do(context.TODO())
			if err != nil {
				return err
			}
			return err
		} else if action == "archive" {
			_, err := r.client.Update().
				Index("map").
				Type("goal").
				Id(goal.Id).
				Script(elastic.NewScriptInline("ctx._source.archived = params.archived").
					Lang("painless").
					Param("archived", true)).
				Do(context.TODO())
			if err != nil {
				return err
			}
		}
		return nil
	}
}

func (r *GoalCommandRepo) DeleteById(id string) error {
	_, err := r.client.Delete().
		Index("map").
		Type("goal").
		Id(id).
		Do(context.TODO())
	if err != nil {
		return err
	}
	return err
}

func (r *GoalMapCommandRepo) Create(goalMap *domain.GoalMap) error{
	_, err := r.client.Index().
		Index("map").
		Type("goal").
		Id(goalMap.Id).
		BodyJson(goalMap).
		Refresh("true").
		Do(context.TODO())

	return err
}

func (r *GoalMapCommandRepo) DeleteById(id string) error {
	_, err := r.client.Delete().
		Index("map").
		Type("goal").
		Id(id).
		Do(context.TODO())
	if err != nil {
		return err
	}
	return err
}