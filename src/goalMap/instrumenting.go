package goalMap

import (
	"github.com/go-kit/kit/metrics"
	"time"
)

// Additional structures
type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	requestError   metrics.Counter
	Service
}

// Additional functions
func NewInstrumentingService(counter metrics.Counter, latency metrics.Histogram, counterE metrics.Counter, s Service) Service {
	return &instrumentingService{
		requestCount:   counter,
		requestLatency: latency,
		requestError:   counterE,
		Service:        s,
	}
}

// ------------------------- Instrumenting functions -------------------------


// Get goals instrumenting service
func (s *instrumentingService) GetGoal(req *GetGoalRequest) (_ *GetGoalResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "get_posts").Add(1)
		if err != nil {
			s.requestError.With("method", "get_posts").Add(1)
		}
		s.requestLatency.With("method", "get_posts").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetGoal(req)
}

// Edit goal instrumenting service
func (s *instrumentingService) EditGoal(req *EditGoalRequest) (_ *EditGoalResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "edit_goal").Add(1)
		if err != nil {
			s.requestError.With("method", "edit_goal").Add(1)
		}
		s.requestLatency.With("method", "edit_goal").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.EditGoal(req)
}


// Delete goal instrumenting service
func (s *instrumentingService) DeleteGoal(req *DeleteGoalRequest) (_ *DeleteGoalResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "delete_goal").Add(1)
		if err != nil {
			s.requestError.With("method", "delete_goal").Add(1)
		}
		s.requestLatency.With("method", "delete_goal").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DeleteGoal(req)
}

// Create/Update goal map instrumenting service
func (s *instrumentingService) CreateGoalMap(req *CreateGoalMapRequest) (_ *CreateGoalMapResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "create_goal_map").Add(1)
		if err != nil {
			s.requestError.With("method", "create_goal_map").Add(1)
		}
		s.requestLatency.With("method", "create_goal_map").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.CreateGoalMap(req)
}

// Delete goal map instrumenting service
func (s *instrumentingService) DeleteGoalMap(req *DeleteGoalMapRequest) (_ *DeleteGoalMapResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "delete_goal_map").Add(1)
		if err != nil {
			s.requestError.With("method", "delete_goal_map").Add(1)
		}
		s.requestLatency.With("method", "delete_goal_map").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DeleteGoalMap(req)
}


