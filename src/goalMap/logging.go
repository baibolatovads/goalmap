package goalMap

import (
	"github.com/go-kit/kit/log"
	"time"
)

// Additional structures
type loggingService struct {
	logger log.Logger
	Service
}

// Additional functions
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

// ------------------------- Logging functions -------------------------


// Edit goal logging service
func (s *loggingService) EditGoal(req *EditGoalRequest) (_ *EditGoalResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "edit_goal",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.EditGoal(req)
}

// Get goals logging service
func (s *loggingService) GetGoal(req *GetGoalRequest) (_ *GetGoalResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "get_posts",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.GetGoal(req)
}



// Delete goal logging service
func (s *loggingService) DeleteGoal(req *DeleteGoalRequest) (_ *DeleteGoalResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "delete_goal",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.DeleteGoal(req)
}

// Create/Update goal map logging service
func (s *loggingService) CreateGoalMap(req *CreateGoalMapRequest) (_ *CreateGoalMapResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "create_goal_map",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.CreateGoalMap(req)
}

// Delete goal map logging service
func (s *loggingService) DeleteGoalMap(req *DeleteGoalMapRequest) (_ *DeleteGoalMapResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "delete_goal_map",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.DeleteGoalMap(req)
}