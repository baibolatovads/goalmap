package goalMap

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"gitlab.com/baibolatovads/goalmap/src/domain"
)

// Goal endpoints

// Get goal req & resp

type GetGoalRequest struct{
	GoalId string `json:"goal_id"`
}

type GetGoalResponse struct{
	domain.Goal
}

// Get goal endpoint
func MakeGetGoalEndpoint(s Service) endpoint.Endpoint{
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetGoalRequest)
		resp, err := s.GetGoal(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}

// Edit goal req & resp
type EditGoalRequest struct{
	domain.Goal
	action string
}

type EditGoalResponse struct{
	Msg string `json:"msg"`
}

// Edit goal endpoint
func EditGoalEndpoint(s Service) endpoint.Endpoint{
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EditGoalRequest)
		resp, err := s.EditGoal(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}

// Delete goal req & resp
type DeleteGoalRequest struct{
	GoalId string `json:"goal_id"`
}

type DeleteGoalResponse struct{
	Msg string `json:"msg"`
}

// Delete goal endpoint
func makeDeleteGoalEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteGoalRequest)
		resp, err := s.DeleteGoal(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}


// Create/Update goal req & resp

type CreateGoalMapRequest struct{
	domain.GoalMap
}

type CreateGoalMapResponse struct{
	GoalId string `json:"goal_id"`
}

//Create Goal Map endpoint
func MakeCreateGoalMapEndpoint(s Service) endpoint.Endpoint{
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateGoalMapRequest)
		resp, err := s.CreateGoalMap(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}

// Delete goal map req & resp
type DeleteGoalMapRequest struct{
	GoalMapId string `json:"goal_id"`
}

type DeleteGoalMapResponse struct{
	Msg string `json:"msg"`
}

// Delete goal map endpoint
func makeDeleteGoalMapEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteGoalMapRequest)
		resp, err := s.DeleteGoalMap(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}

