package goalMap

import (
	"context"
	"gitlab.com/baibolatovads/goalmap/src/errors"
	"net/http"
	"encoding/json"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

)

// Http routes handlers
func MakeHandler(ss Service) http.Handler {

	// Options array
	options := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}

	// Blog routes handlers

	getGoal := kithttp.NewServer(
		MakeGetGoalEndpoint(ss),
		decodeGetGoalRequest,
		encodeResponse,
		options...,
	)

	deleteGoal := kithttp.NewServer(
		makeDeleteGoalEndpoint(ss),
		decodeDeleteGoalRequest,
		encodeResponse,
		options...,
	)

	editGoal := kithttp.NewServer(
		EditGoalEndpoint(ss),
		decodeEditGoalRequest,
		encodeResponse,
		options...,
		)

	createGoalMap := kithttp.NewServer(
		MakeCreateGoalMapEndpoint(ss),
		decodeCreateGoalMapRequest,
		encodeResponse,
		options...,
	)

	deleteGoalMap := kithttp.NewServer(
		makeDeleteGoalMapEndpoint(ss),
		decodeDeleteGoalMapRequest,
		encodeResponse,
		options...,
	)

	// Init router
	r := mux.NewRouter()

	// Goal Map routes
	r.Handle("/goal-map/goal/{id}", editGoal).Methods("PUT", "POST")
	r.Handle("/goal-map/goal/{id}", getGoal).Methods("GET")
	r.Handle("/goal-map/goal/{id}", deleteGoal).Methods("DELETE")
	r.Handle("/goal-map/goal-sample", createGoalMap).Methods("PUT", "POST")
	r.Handle("/goal-map/goal-sample/{id}", deleteGoalMap).Methods("DELETE")

	return r
}

// Goal Map request decoders ------------------------------------


// Edit goal request decoder
func decodeEditGoalRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body EditGoalRequest

	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	return body, nil
}

// Get goals request decoder
func decodeGetGoalRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body GetGoalRequest
	vars := mux.Vars(r)

	id, ok := vars["id"]
	if !ok {
		return nil, errors.NoFound
	}

	body.GoalId = id

	return body, nil
}



// Delete goal request decoder
func decodeDeleteGoalRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body DeleteGoalRequest
	vars := mux.Vars(r)

	id, ok := vars["id"]
	if !ok {
		return nil, errors.NoFound
	}

	body.GoalId = id

	return body, nil
}


// Create/Update goal map request decoder
func decodeCreateGoalMapRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body CreateGoalMapRequest

	// Parse request
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	// Validate request
	if body.Id == "" {
		return nil, errors.InvalidCharacter.DevMessage("goal id must be provided")
	}
	if body.Questions == nil {
		return nil, errors.InvalidCharacter.DevMessage("goal questions must be provided")
	}

	return body, nil
}

// Delete goal map request decoder
func decodeDeleteGoalMapRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body DeleteGoalMapRequest
	vars := mux.Vars(r)

	id, ok := vars["id"]
	if !ok {
		return nil, errors.NoFound
	}

	body.GoalMapId = id

	return body, nil
}

// Response encoders
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	e, ok := response.(errorer)
	if ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// Error response encoder
type errorer interface {
	error() error
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	e, ok := err.(*errors.ArgError)
	if ok {
		w.WriteHeader(e.Status)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	_ = json.NewEncoder(w).Encode(err)
}
