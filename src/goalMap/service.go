package goalMap

import "gitlab.com/baibolatovads/goalmap/src/domain"

// //////////////////////////////////////
// Main service interface & constructor
// //////////////////////////////////////

type Service interface {
	GetGoal(req *GetGoalRequest) (*GetGoalResponse, error)
	DeleteGoal(req *DeleteGoalRequest) (*DeleteGoalResponse, error)
	EditGoal(req *EditGoalRequest) (*EditGoalResponse, error)
	CreateGoalMap(req *CreateGoalMapRequest)(*CreateGoalMapResponse, error)
	DeleteGoalMap(req *DeleteGoalMapRequest)(*DeleteGoalMapResponse, error)
}

type service struct{
	goalCommandRepo domain.GoalCommandRepo
	goalMapCommandRepo domain.GoalMapCommandRepo
}

func NewService(goalCR domain.GoalCommandRepo, goalMapCR domain.GoalMapCommandRepo) Service{
	return &service{
		goalCommandRepo: goalCR,
		goalMapCommandRepo: goalMapCR,
	}
}


// Goal Map service methods ------------------------------------------------------

// Create/Update goal service method

// Get goals service method
func (s *service) GetGoal(req *GetGoalRequest) (*GetGoalResponse, error){
	goal, err := s.goalCommandRepo.GetById(req.GoalId)
	if err != nil {
		return nil, err
	}
	return &GetGoalResponse{*goal}, nil
}

// Edit goal service method
func (s * service) EditGoal(req *EditGoalRequest) (*EditGoalResponse, error){
	goal := req.Goal
	action := req.action
	err := s.goalCommandRepo.EditGoal(action, &goal)
	if err != nil {
		return nil, err
	}
	return &EditGoalResponse{Msg:"Successfully updated"}, nil
}

// Delete goals service method
func (s * service) DeleteGoal(req *DeleteGoalRequest) (*DeleteGoalResponse, error){
	err := s.goalCommandRepo.DeleteById(req.GoalId)
	if err != nil {
		return nil, err
	}
	return &DeleteGoalResponse{Msg: "Successfully deleted"}, nil
}

// Create/Update goal map service method

func (s *service) CreateGoalMap(req *CreateGoalMapRequest) (*CreateGoalMapResponse, error) {

	goalMap := req.GoalMap

	if goalMap.Id == "" {
		goalMap.GenerateGoalId()
	}

	err := s.goalMapCommandRepo.Create(&goalMap)
	if err != nil {
		return nil, err
	}

	return &CreateGoalMapResponse{GoalId: goalMap.Id}, nil
}

// Delete goals service method
func (s * service) DeleteGoalMap(req *DeleteGoalMapRequest) (*DeleteGoalMapResponse, error){
	err := s.goalCommandRepo.DeleteById(req.GoalMapId)
	if err != nil {
		return nil, err
	}
	return &DeleteGoalMapResponse{Msg: "Successfully deleted"}, nil
}
