package domain

import "github.com/google/uuid"

// ///////////////////////////////
// Main structure & methods
// ///////////////////////////////

type Goal struct{
	Id string `json:"id"`
	Title string `json:"title"`
	Description string `json:"description"`
	Status bool `json:"status"`
	Archived bool `json:"archived"`
	GoalMapId bool `json:"goalMap_id"`
	Answers bool `json:"answers"`
}

type GoalMap struct{
	Id string `json:"id"`
	Questions []string `json:"questions"`
}

// Generate goal's unique ID
func (goal *Goal) GenerateId(){
	goal.Id = uuid.New().String()
}

// Generate goal map's unique ID
func (goalMap *GoalMap) GenerateGoalId(){
	goalMap.Id = uuid.New().String()
}


type GoalCommandRepo interface {
	GetById(id string) (*Goal, error)
	DeleteById(id string) error
	EditGoal(action string, goal *Goal) error
}

type GoalMapCommandRepo interface{
	Create(goalMap *GoalMap) error
	DeleteById(id string) error
}
